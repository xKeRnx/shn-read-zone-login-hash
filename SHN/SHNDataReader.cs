﻿using System.IO;
using System.Text;

namespace SHN
{
    public class SHNDataReader : BinaryReader
    {
        public SHNDataReader(Stream input)
          : base(input)
        {
        }

        public string ReadPaddedString(int length)
        {
            string empty = string.Empty;
            int count = 0;
            byte[] bytes = this.ReadBytes(length);
            while (count < length && bytes[count] != (byte)0)
                ++count;
            if (length > 0)
                empty = Encoding.UTF8.GetString(bytes, 0, count);
            return empty;
        }

        public long Seek(long offset, SeekOrigin origin)
        {
            return this.BaseStream.Seek(offset, origin);
        }

        public long Skip(long offset)
        {
            return this.Seek(offset, SeekOrigin.Current);
        }
    }
}
