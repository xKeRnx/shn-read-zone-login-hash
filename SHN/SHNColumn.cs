﻿using System.Data;

namespace SHN
{
    public class SHNColumn : DataColumn
    {
        public int Length { get; }

        public byte Type { get; }

        public SHNColumn(SHNDataReader reader, ref int unkColumnCount)
        {
            string str = reader.ReadPaddedString(48);
            if (str.Trim().Length < 2)
            {
                this.ColumnName = string.Format("UnkCol{0}", (object)unkColumnCount);
                ++unkColumnCount;
            }
            else
                this.ColumnName = str;
            this.Type = (byte)reader.ReadUInt32();
            this.DataType = SHNColumn.GetType(this.Type);
            this.Length = reader.ReadInt32();
        }

        public static System.Type GetType(byte type)
        {
            switch (type)
            {
                case 1:
                case 12:
                    return typeof(byte);
                case 2:
                    return typeof(ushort);
                case 3:
                case 11:
                    return typeof(uint);
                case 5:
                    return typeof(float);
                case 9:
                case 24:
                case 26:
                    return typeof(string);
                case 13:
                case 21:
                    return typeof(short);
                case 16:
                    return typeof(byte);
                case 18:
                case 27:
                    return typeof(uint);
                case 20:
                    return typeof(sbyte);
                case 22:
                    return typeof(int);
                default:
                    return typeof(object);
            }
        }
    }
}
