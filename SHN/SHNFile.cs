﻿using System;
using System.Data;
using System.IO;

namespace SHN
{
    public class SHNFile : DataTable
    {
        private uint DefaultRowLength;

        public uint ColumnCount { get; private set; }

        public uint RowCount { get; private set; }

        public string FileName { get; }

        public uint Header { get; private set; }

        public byte[] CryptHeader { get; private set; }

        public byte[] ChecksumData { get; set; }

        public SHNFile(string filePath)
        {
            this.FileName = filePath;
            this.TableName = Path.GetFileName(filePath);
            this.Read();
        }

        private void Read()
        {
            if (!File.Exists(this.FileName))
                throw new FileNotFoundException("The file could not be found", this.FileName);
            this.ChecksumData = File.ReadAllBytes(this.FileName);
            byte[] buffer;
            using (FileStream fileStream = File.Open(this.FileName, FileMode.Open, FileAccess.Read, FileShare.Read))
            {
                using (BinaryReader binaryReader = new BinaryReader((Stream)fileStream))
                {
                    this.CryptHeader = binaryReader.ReadBytes(32);
                    int count = binaryReader.ReadInt32() - 36;
                    buffer = binaryReader.ReadBytes(count);
                    byte num = (byte)count;
                    for (int index = count - 1; index >= 0; --index)
                    {
                        buffer[index] = (byte)((uint)buffer[index] ^ (uint)num);
                        this.ChecksumData[index + 36] = (byte)((uint)this.ChecksumData[index + 36] ^ (uint)num);
                        num = (byte)((uint)(byte)((uint)(byte)((uint)(byte)((uint)(byte)((uint)(byte)index & 15U) + 85U) ^ (uint)(byte)((uint)(byte)index * 11U)) ^ (uint)num) ^ 170U);
                    }
                }
            }
            using (MemoryStream memoryStream = new MemoryStream(buffer))
            {
                using (SHNDataReader reader = new SHNDataReader((Stream)memoryStream))
                {
                    this.Header = reader.ReadUInt32();
                    this.RowCount = reader.ReadUInt32();
                    this.DefaultRowLength = reader.ReadUInt32();
                    this.ColumnCount = reader.ReadUInt32();
                    int unkColumnCount = 0;
                    int num1 = 2;
                    for (int index = 0; (long)index < (long)this.ColumnCount; ++index)
                    {
                        SHNColumn shnColumn = new SHNColumn(reader, ref unkColumnCount);
                        num1 += shnColumn.Length;
                        this.Columns.Add((DataColumn)shnColumn);
                    }
                    if ((long)num1 != (long)this.DefaultRowLength)
                        throw new Exception("The default row length does not fit");
                    object[] objArray = new object[(int)this.ColumnCount];
                    for (uint index1 = 0; index1 < this.RowCount; ++index1)
                    {
                        uint num2 = (uint)reader.ReadUInt16();
                        for (int index2 = 0; (long)index2 < (long)this.ColumnCount; ++index2)
                        {
                            SHNColumn column = (SHNColumn)this.Columns[index2];
                            switch (column.Type)
                            {
                                case 1:
                                case 12:
                                case 16:
                                    objArray[index2] = (object)reader.ReadByte();
                                    break;
                                case 2:
                                    objArray[index2] = (object)reader.ReadUInt16();
                                    break;
                                case 3:
                                case 11:
                                case 18:
                                case 27:
                                    objArray[index2] = (object)reader.ReadUInt32();
                                    break;
                                case 5:
                                    objArray[index2] = (object)reader.ReadSingle();
                                    break;
                                case 9:
                                case 24:
                                    objArray[index2] = (object)reader.ReadPaddedString(column.Length);
                                    break;
                                case 13:
                                case 21:
                                    objArray[index2] = (object)reader.ReadInt16();
                                    break;
                                case 20:
                                    objArray[index2] = (object)reader.ReadSByte();
                                    break;
                                case 22:
                                    objArray[index2] = (object)reader.ReadInt32();
                                    break;
                                case 26:
                                    objArray[index2] = (object)reader.ReadPaddedString((int)num2 - (int)this.DefaultRowLength + 1);
                                    break;
                                case 29:
                                    byte[] numArray = reader.ReadBytes(column.Length);
                                    uint uint32_1 = BitConverter.ToUInt32(numArray, 0);
                                    uint uint32_2 = BitConverter.ToUInt32(numArray, 4);
                                    objArray[index2] = (object)new Program.Pair<uint, uint>(uint32_1, uint32_2);
                                    break;
                                default:
                                    throw new Exception("New SHN column type found");
                            }
                        }
                        this.Rows.Add(objArray);
                    }
                }
            }
        }
    }
}
