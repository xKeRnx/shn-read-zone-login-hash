﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;

namespace SHN
{
    class Program
    {
        public class Pair<T1, T2>
        {
            public T1 First { get; set; }

            public T2 Second { get; set; }

            public Pair(T1 first, T2 second)
            {
                this.First = first;
                this.Second = second;
            }
        }

        public static class SHNFileCheckSum
        {
            public static string GetCheckSum(string fileName)
            {
                using (MD5 md5 = MD5.Create())
                {
                    using (SHNFile shnFile = new SHNFile(fileName))
                        return BitConverter.ToString(md5.ComputeHash(shnFile.ChecksumData)).Replace("-", "").ToLower();
                }
            }
        }

        public enum SHN_DATA_FILE_INDEX
        {
            SHN_Abstate = 0,
            SHN_ActiveSkill = 1,
            SHN_CharacterTitleData = 2,
            SHN_ChargedEffect = 3,
            SHN_ClassName = 4,
            SHN_Gather = 5,
            SHN_GradeItemOption = 6,
            SHN_ItemDismantle = 7,
            SHN_ItemInfo = 8,
            SHN_MapInfo = 9,
            SHN_MiniHouse = 10,
            SHN_MiniHouseFurniture = 11,
            SHN_MiniHouseObjAni = 12,
            SHN_MobInfo = 13,
            SHN_PassiveSkill = 14,
            SHN_Riding = 15,
            SHN_SubAbstate = 16,
            SHN_UpgradeInfo = 17,
            SHN_WeaponAttrib = 18,
            SHN_WeaponTitleData = 19,
            SHN_MiniHouseFurnitureObjEffect = 20,
            SHN_MiniHouseFurnitureEndure = 21,
            SHN_DiceDividind = 22,
            SHN_ActionViewInfo = 23,
            SHN_MapLinkPoint = 24,
            SHN_MapWayPoint = 25,
            SHN_AbStateView = 26,
            SHN_ActiveSkillView = 27,
            SHN_CharacterTitleStateView = 28,
            SHN_EffectViewInfo = 29,
            SHN_ItemShopView = 30,
            SHN_ItemViewInfo = 31,
            SHN_MapViewInfo = 32,
            SHN_MobViewInfo = 33,
            SHN_NPCViewInfo = 34,
            SHN_PassiveSkillView = 35,
            SHN_ProduceView = 36,
            SHN_CollectCardView = 37,
            SHN_GTIView = 38,
            SHN_ItemViewEquipTypeInfo = 39,
            SHN_SingleData = 40,
            SHN_MarketSearchInfo = 41,
            SHN_ItemMoney = 42,
            SHN_PupMain = 43,
            SHN_ChatColor = 44,
            SHN_TermExtendMatch = 45,
            SHN_MinimonInfo = 46,
            SHN_MinimonAutoUseItem = 47,
            SHN_ChargedDeletableBuff = 48,
            SHN_SlanderFilter = 49,
            SHN_MaxCnt = 50
        }

        public static string ShinePath;

        static void Main(string[] args)
        {
            ShinePath = Directory.GetCurrentDirectory() + @"\ressystem\";
            if (!Directory.Exists(ShinePath)) {
                Directory.CreateDirectory(ShinePath);
            }
            var builder = new StringBuilder();
            for (int index = 0; index < Enum.GetNames(typeof(SHN_DATA_FILE_INDEX)).Length -1; ++index)
            {
                string str = string.Format("{0}{1}.shn", (object)ShinePath, (object)((SHN_DATA_FILE_INDEX)index).ToString().Replace("SHN_", ""));
                if (File.Exists(str))
                    builder.Append(SHNFileCheckSum.GetCheckSum(str));
            }
            Console.WriteLine(builder.ToString());
            Console.ReadLine();
        }
    }
}
